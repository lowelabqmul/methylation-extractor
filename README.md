# README #

### What is this repository for? ###

* Provides matrix count data for methylation alignments from BISMARK
* Can use either BAM or SAM alignments
* Can combine multiple files into one table
* V1

### How do I get set up? ###

* Cd to 
* run

```
#!bash

g++ main.cpp -o methylation-extractor
```

### How do run? ###


```
#!bash

./methylation-extractor -i <file_selector> -d <directory> -c <n1> -t <n2>
```

<file_selector> - is a string which can be used to filter out which files get used. E.g. simply set to .sam or .bam
 
<directory> - location containing all of the appropriate files

<n1> (optional) - the number of bases to ignore from the beginning of the read (useful for RRBS data)

<n2> (optional) - the number of bases to ignore from the end of each read
