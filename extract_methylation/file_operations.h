//
//  file_operations.h
//  tDMR_with_450K
//
//  Created by Robert Lowe on 14/08/2012.
//  Copyright (c) 2012 Robert Lowe. All rights reserved.
//

#ifndef tDMR_with_450K_file_operations_h
#define tDMR_with_450K_file_operations_h

#include <vector>

std::vector<std::string> split_line(std::string line,std::string split){
    int pos;
    std::vector<std::string> tokens;
    while( (pos = (int)line.find(split))!=std::string::npos)
    {
        std::string line2=line.substr(0,pos);
        tokens.push_back(line2);
        line = line.substr(pos+1);
    }
    tokens.push_back(line);
    
    return(tokens);
}


std::vector<std::string> tab_split_line(std::string line){
    int pos;
    std::vector<std::string> tokens;
    while( (pos = (int)line.find("\t"))!=std::string::npos)
    {
        std::string line2=line.substr(0,pos);
        tokens.push_back(line2);
        line = line.substr(pos+1);
    }
    tokens.push_back(line);
    
    return(tokens);
}

std::vector<std::string> comma_split_line(std::string line){
    int pos;
    std::vector<std::string> tokens;
    while( (pos = (int)line.find(","))!=std::string::npos)
    {
        std::string line2=line.substr(0,pos);
        tokens.push_back(line2);
        line = line.substr(pos+1);
    }
    tokens.push_back(line);
    
    return(tokens);
}


#endif
