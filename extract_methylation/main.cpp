//
//  main.cpp
//  extract_methylation
//
//  Created by Robert Lowe on 01/07/2013.
//  Copyright (c) 2013 Robert Lowe. All rights reserved.
//
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <dirent.h>
#include <stdio.h>      /* printf, fgets */
#include <stdlib.h>
#include <getopt.h>
#include "file_operations.h"

struct methylation{
    std::string chr;
    int pos;
    int meth;
};

std::vector<methylation> get_CpG_meth(std::vector<std::string> &read, std::vector<int>&index,int clip,int tail,int meth_column){
    
    int end;
    if(tail==0){
        end=(int)read[meth_column].size();
    }else{
        end=tail+5;
    }
    std::vector<methylation> vector_meth;
    for(int i=5+clip; i<end; i++){
        if(index[i-5]!=-1){
            methylation temp;
            temp.chr=read[2];
            if(read[1]=="99" || read[1]=="147" || read[1]=="131" || read[1]=="67" || read[1]=="0" ){ //Top Strand 
                temp.pos=atoi(read[3].c_str())+index[i-5];
            }
            else if(read[1]=="115"|| read[1]=="179" || read[1]=="83" || read[1]=="163" || read[1]=="16"){ //Bottom Strand
                temp.pos=atoi(read[3].c_str())+index[i-5]-1;
            }
            else{
                std::cout << read[0] << std::endl;
                std::cout << "Flag not supported: " << read[1] << std::endl;
                exit(0);
            }
            
            //Don't Include Cases where there has been an insert or deletion to create CpG
            int ok=0;
            if(i-6>=0){
                if(index[i-5]-index[i-6]!=1){
                    ok=1;
                }
            }
            if(i-4<=index.size()){
                if(index[i-4]-index[i-5]!=1){
                    ok=1;
                }
            }

            
            if(read[meth_column][i]=='z'){
                temp.meth=0;
                if(ok==0)
                    vector_meth.push_back(temp);
                
                
            }
            else if (read[meth_column][i]=='Z'){
                temp.meth=1;
                if(ok==0)
                    vector_meth.push_back(temp);
            }
        }
        
    }
    
    return(vector_meth);
}


std::vector<int> get_index(std::string CIGAR,int size){
    
    std::vector<int> index;
    int prev=0;
    int current=0;
    for(int z=0; z<CIGAR.size(); z++){
        if(CIGAR[z]=='M'){
            for(int k=current; k<current+atoi(CIGAR.substr(prev,z-prev).c_str()); k++){
                index.push_back(k);
            }
            current=current+atoi(CIGAR.substr(prev,z-prev).c_str());
            prev=z+1;
        }
        if(CIGAR[z]=='D'){
            current+=atoi(CIGAR.substr(prev,z-prev).c_str());
            prev=z+1;
        }
        if(CIGAR[z]=='I'){
            for(int k=current; k<current+atoi(CIGAR.substr(prev,z-prev).c_str()); k++){
                index.push_back(-1);
            }
            prev=z+1;
        }
        
    }
    if(index.size()!=size){
        std::cout << size  << std::endl;
        std::cerr << CIGAR << std::endl;
        exit(0);
    }


    return(index);
}

void combine_samples(std::vector<methylation> &r1_meth, std::vector<methylation> &r2_meth, std::map<std::string,std::map<int,std::vector<int> > > &meth, std::map<std::string,std::map<int,std::vector<int> > > &total,std::vector<int> inital_samples,int index_sample){
    
    std::map<int,int> meth_temp;
    
    for(int i=0; i<r1_meth.size(); i++){
        meth_temp[r1_meth[i].pos]=r1_meth[i].meth;
    }
    for(int i=0; i<r2_meth.size(); i++){
        if(meth_temp.count(r2_meth[i].pos)==0){
            meth_temp[r2_meth[i].pos]=r2_meth[i].meth;
        }
    }
    
    std::string chr="";
    if(r1_meth.size()!=0){
        chr=r1_meth[0].chr;
    }else if(r2_meth.size()!=0){
        chr=r2_meth[0].chr;
    }
    
    for(std::map<int,int>::iterator it=meth_temp.begin(); it!=meth_temp.end(); it++){
        if(chr==""){
            std::cout << "ERROR: CHR not set" << std::endl;
        }
        if(meth.count(chr)==0){
            meth[chr]=std::map<int,std::vector<int> > ();
            total[chr]=std::map<int,std::vector<int> > ();
        }
        if(meth[chr].count(it->first)==0){
            meth[chr][it->first]=inital_samples;
            total[chr][it->first]=inital_samples;
        }
        if(it->second==1){
            meth[chr][it->first][index_sample]+=1;
        }
        total[chr][it->first][index_sample]+=1;
    }
}

void update_meth_status(std::vector<methylation> &r1_meth, std::map<std::string,std::map<int,std::vector<int> > > &meth, std::map<std::string,std::map<int,std::vector<int> > > &total,std::vector<int> inital_samples,int index_sample){
    
    std::map<int,int> meth_temp;
    
    for(int i=0; i<r1_meth.size(); i++){
        meth_temp[r1_meth[i].pos]=r1_meth[i].meth;
    }

    
    std::string chr="";
    if(r1_meth.size()!=0){
        chr=r1_meth[0].chr;
    }
    
    for(std::map<int,int>::iterator it=meth_temp.begin(); it!=meth_temp.end(); it++){
        if(chr==""){
            std::cout << "ERROR: CHR not set" << std::endl;
        }
        if(meth.count(chr)==0){
            meth[chr]=std::map<int,std::vector<int> > ();
            total[chr]=std::map<int,std::vector<int> > ();
        }
        if(meth[chr].count(it->first)==0){
            meth[chr][it->first]=inital_samples;
            total[chr][it->first]=inital_samples;
        }
        if(it->second==1){
            meth[chr][it->first][index_sample]+=1;
        }
        total[chr][it->first][index_sample]+=1;
    }
}



void meth_extractor(std::vector<std::string> &r1, std::vector<std::string> &r2, std::map<std::string,std::map<int,std::vector<int> > > &meth, std::map<std::string,std::map<int,std::vector<int> > > &total,int number_of_samples,int index,int clip,int tail,int &meth_column){
    
    //Deal with deletions/Insertions
    if(meth_column==0){
        for(int i=0; i<r1.size(); i++){
            if(r1[i].size()>4)
                if(r1[i].substr(0,4)=="XM:Z")
                    meth_column=i;
        }
    }
    std::vector<int> r1_index=get_index(r1[5],(int)r1[meth_column].size()-5);
    std::vector<int> r2_index=get_index(r2[5],(int)r2[meth_column].size()-5);
    
    std::vector<methylation> r1_meth=get_CpG_meth(r1,r1_index,clip,tail,meth_column);
    std::vector<methylation> r2_meth=get_CpG_meth(r2,r2_index,clip,tail,meth_column);
    

    if(r1_meth.size()!=0 and r2_meth.size()!=0){
        if(r1_meth[0].chr!=r2_meth[0].chr){
            std::cout << "Misaligned Chromosomes " << r1[0] << " " << r2[0] << std::endl;
            exit(0);
        }
    }
    
    std::vector<int> initial_samples;
    for(int i=0; i<number_of_samples; i++){
        initial_samples.push_back(0);
    }
    
    combine_samples(r1_meth,r2_meth,meth,total,initial_samples,index);
    

    
    
}



void meth_extractor_single(std::vector<std::string> &r1, std::map<std::string,std::map<int,std::vector<int> > > &meth, std::map<std::string,std::map<int,std::vector<int> > > &total,int number_of_samples,int index,int clip,int tail,int &meth_column){
    
    //Deal with deletions/Insertions
    if(meth_column==0){
        for(int i=0; i<r1.size(); i++){
            if(r1[i].size()>4)
                if(r1[i].substr(0,4)=="XM:Z")
                    meth_column=i;
        }
    }
    std::vector<int> r1_index=get_index(r1[5],(int)r1[meth_column].size()-5);
    
    std::vector<methylation> r1_meth=get_CpG_meth(r1,r1_index,clip,tail,meth_column);
    
    
    std::vector<int> initial_samples;
    for(int i=0; i<number_of_samples; i++){
        initial_samples.push_back(0);
    }
    
    update_meth_status(r1_meth,meth,total,initial_samples,index);
    
    
    
    
}


void run_extraction_bam(std::map<std::string,std::map<int,std::vector<int> > > &meth, std::map<std::string,std::map<int,std::vector<int> > > &total, std::vector<std::string> list_files,int clip,int tail,bool single_end){
    
    int number_of_samples=(int)list_files.size();
    int meth_column=0;
    for(int i=0; i<list_files.size(); i++){
        std::cerr << list_files[i] << std::endl;
        std::string filename=list_files[i];
        const int MAX_BUFFER = 2048;
        std::stringstream ss;
        ss << "samtools view " << filename;
        std::string cmd=ss.str();
    
        char buffer[MAX_BUFFER];
        FILE *stream = popen(cmd.c_str(), "r");
        if (stream){
            int readp=0;
            std::vector<std::string> r1;
            while (!feof(stream))
            {
                if (fgets(buffer, MAX_BUFFER, stream) != NULL)
                {
                    std::vector<std::string> tokens=tab_split_line(buffer);
                    if(readp==0){
                        r1=tokens;
                        if(single_end){
                            meth_extractor_single(r1,meth,total,number_of_samples,i,clip,tail,meth_column);
                            readp=-1;
                        }
                    }
                    if(readp==1){
                        //Handle /1 and /2 on end of read names
                        if(r1[0].substr(r1[0].size()-2,2)=="/1" and tokens[0].substr(tokens[0].size()-2,2)=="/2"){
                            if(r1[0].substr(0,r1[0].size()-2)!=tokens[0].substr(0,tokens[0].size()-2)){
                                std::cout << "File is not sorted by name" << std::endl;
                                std::cout << r1[0].substr(0,r1[0].size()-2) << " != " << tokens[0].substr(0,tokens[0].size()-2) << std::endl;
                                exit(0);
                            }
                            else{
                                meth_extractor(r1, tokens,meth,total,number_of_samples,i,clip,tail,meth_column);
                                
                            }
                        }
                        else{
                            if(r1[0]!=tokens[0]){
                                std::cout << "File is not sorted by name" << std::endl;
                                std::cout << r1[0] << " != " << tokens[0] << std::endl;
                                exit(0);
                            }
                            else{
                                meth_extractor(r1, tokens,meth,total,number_of_samples,i,clip,tail,meth_column);
                            }
                        }
                        readp=-1;
                    }
                    readp++;
                }
            }
        }
        else{
            std::cout << "Couldn't open file " << list_files[i] << std::endl;
            exit(0);
        }
    }
}

void run_extraction_sam(std::map<std::string,std::map<int,std::vector<int> > > &meth, std::map<std::string,std::map<int,std::vector<int> > > &total, std::vector<std::string> list_files,int clip,int tail,bool single_end){
    
    int number_of_samples=(int)list_files.size();
    int meth_column=0;

    for(int i=0; i<list_files.size(); i++){
        std::cerr << list_files[i] << std::endl;
        std::ifstream myfile(list_files[i].c_str());
        if(myfile.is_open()){
            std::string line;
            int readp=0;
            std::vector<std::string> r1;
            while(getline(myfile,line)){
                if(line.find("@")!=0){
                    std::vector<std::string> tokens=tab_split_line(line);
                    if(readp==0){
                        r1=tokens;
                        if(single_end){
                            meth_extractor_single(r1,meth,total,number_of_samples,i,clip,tail,meth_column);
                            readp=-1;
                        }
                    }
                    if(readp==1){
                        //Handle /1 and /2 on end of read names
                        if(r1[0].substr(r1[0].size()-2,2)=="/1" and tokens[0].substr(tokens[0].size()-2,2)=="/2"){
                            if(r1[0].substr(0,r1[0].size()-2)!=tokens[0].substr(0,tokens[0].size()-2)){
                                std::cout << "File is not sorted by name" << std::endl;
                                std::cout << r1[0].substr(0,r1[0].size()-2) << " != " << tokens[0].substr(0,tokens[0].size()-2) << std::endl;
                                exit(0);
                            }
                            else{
                                meth_extractor(r1, tokens,meth,total,number_of_samples,i,clip,tail,meth_column);

                            }
                        }
                        else{
                            if(r1[0]!=tokens[0]){
                                std::cout << "File is not sorted by name" << std::endl;
                                std::cout << r1[0] << " != " << tokens[0] << std::endl;
                                exit(0);
                            }
                            else{
                                meth_extractor(r1, tokens,meth,total,number_of_samples,i,clip,tail,meth_column);
                            }
                        }
                        readp=-1;
                    }
                    readp++;
                }
            }
        }
        else{
            std::cout << "Couldn't open file " << list_files[i] << std::endl;
            exit(0);
        }
    }
    

    
}


void readfile(std::string file_sel,std::string directory,int clip,int tail,bool single_end){


    std::vector<std::string> list_files;
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (directory.c_str())) != NULL) {
        while ((ent = readdir (dir)) != NULL) {
            std::stringstream ss;
            ss << ent->d_name;
            if(ss.str().find(file_sel)!=std::string::npos){
                std::stringstream filename;
                filename << directory << ss.str();
                list_files.push_back(filename.str());
            }
            
        }
        closedir (dir);
    } else {
        perror ("");
        exit(1);
    }
    
    if(list_files.size()==0){
        std::cout << "No files found using " << file_sel << std::endl;
        exit(0);
    }
    
    std::map<std::string,std::map<int,std::vector<int> > > meth;
    std::map<std::string,std::map<int,std::vector<int> > > total;
    
    if(file_sel.find(".sam")!=std::string::npos)
        run_extraction_sam(meth,total,list_files,clip,tail,single_end);
    else if(file_sel.find(".bam")!=std::string::npos)
        run_extraction_bam(meth,total,list_files,clip,tail,single_end);
    else{
        std::cout << "No recognised files using:" << file_sel << std::endl;
        std::cout << "Please include either .sam or .bam" << std::endl;
        exit(0);
    }
    
    
    std::cout << "chr\tpos";
    for(int i=0; i<list_files.size(); i++){
        if(list_files[i].find(file_sel)!=std::string::npos)
            std::cout << "\t" << list_files[i].substr(list_files[i].find_last_of("/")+1,list_files[i].find(file_sel)-list_files[i].find_last_of("/")-1) << "_Meth_Counts\t" << list_files[i].substr(list_files[i].find_last_of("/")+1,list_files[i].find(file_sel)-list_files[i].find_last_of("/")-1) << "_Total_Counts";
    }
    std::cout<< std::endl;
    
    for(std::map<std::string,std::map<int,std::vector<int> > >::iterator itr=meth.begin(); itr!=meth.end(); itr++){
        for(std::map<int,std::vector<int> >::iterator itr2=meth[itr->first].begin(); itr2!=meth[itr->first].end(); itr2++){
            std::cout << itr->first << "\t" << itr2->first;
            for(int i=0; i<meth[itr->first][itr2->first].size(); i++){
                std::cout << "\t" << meth[itr->first][itr2->first][i] << "\t" << total[itr->first][itr2->first][i];
            }
            std::cout << std::endl;
        }
    }
    
    
}


int main(int argc,char* const argv[])
{
    std::string file_sel="";
    std::string directory="";
    bool single_end=false;
    int clip=0,tail=0;
    char ch;
    while ((ch = getopt(argc, argv, "i:d:c:t:s")) != -1) {
		switch (ch) {
			case 'i':
				file_sel=optarg;
				break;
			case 'd':
				directory=optarg;
                break;
            case 'c':
                clip=atoi(optarg);
                break;
            case 't':
                tail=atoi(optarg);
            case 's':
                single_end=true;
            break;
        }
    }
    if(file_sel==""){
        std::cout << "No input files selected" << std::endl;
        exit(0);
    }

    readfile(file_sel,directory,clip,tail,single_end);
    
    return 0;
}

